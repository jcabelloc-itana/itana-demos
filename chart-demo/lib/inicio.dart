import 'package:chart_demo/syncfusion/components/sync_fusion_body.dart';
import 'package:flutter/material.dart';

class Inicio extends StatelessWidget {
  const Inicio({super.key});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Charts con Syncfusion",
          style: TextStyle(
            color: colorScheme.background,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: colorScheme.primary,
      ),
      body: const SyncFusionBody(),
    );
  }
}
