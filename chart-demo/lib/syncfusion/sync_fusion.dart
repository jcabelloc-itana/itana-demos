import 'package:chart_demo/syncfusion/components/sync_fusion_body.dart';
import 'package:flutter/material.dart';

class SyncFusion extends StatelessWidget {
  static const String route = "sync-fusion";
  const SyncFusion({super.key});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Sync Fusion",
          style: TextStyle(
            color: colorScheme.background,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: colorScheme.primary,
      ),
      body: const SyncFusionBody(),
    );
  }
}
