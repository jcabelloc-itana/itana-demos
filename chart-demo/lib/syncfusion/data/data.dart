import 'package:chart_demo/syncfusion/model/chart_data.dart';
import 'package:chart_demo/syncfusion/model/datetime_data.dart';
import 'package:flutter/material.dart';

class Data {
  static List<ChartData> resultados = [
    ChartData('Camisas', 30, Colors.blue),
    ChartData('Zapatillas', 27, Colors.red),
    ChartData('Casacas', 26, Colors.purple),
    ChartData('Pantalones', 33, Colors.yellow),
  ];

  static List<DatetimeData> resultadosWithDatetime = [
    DatetimeData(DateTime(2023, 5), [34, 23, 32, 21]),
    DatetimeData(DateTime(2023, 6), [23, 31, 15, 21]),
    DatetimeData(DateTime(2023, 7), [43, 12, 15, 21]),
    DatetimeData(DateTime(2023, 8), [54, 35, 32, 21]),
    DatetimeData(DateTime(2023, 9), [12, 23, 15, 27]),
    DatetimeData(DateTime(2023, 10), [26, 31, 35, 28]),
  ];
}
