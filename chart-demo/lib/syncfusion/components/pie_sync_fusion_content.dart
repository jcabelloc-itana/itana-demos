import 'package:chart_demo/syncfusion/data/data.dart';
import 'package:chart_demo/syncfusion/model/chart_data.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class PieSyncFusionContent extends StatelessWidget {
  const PieSyncFusionContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SfCircularChart(
          series: <CircularSeries<ChartData, String>>[
            PieSeries<ChartData, String>(
              dataSource: Data.resultados,
              xValueMapper: (ChartData data, _) => data.x,
              yValueMapper: (ChartData data, _) => data.y,
              dataLabelSettings: const DataLabelSettings(
                isVisible: true,
                textStyle: TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                ),
              ),
              radius: '100',
            )
          ],
          palette: const [
            Colors.green,
            Colors.blue,
            Colors.red,
            Colors.orange,
            Colors.yellow,
          ],
          legend: const Legend(
            isVisible: true,
            position: LegendPosition.bottom,
          ),
          title: ChartTitle(
            text: 'Cantidad de Ventas por Producto - Octubre 2023',
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
        ),
      ],
    );
  }
}
