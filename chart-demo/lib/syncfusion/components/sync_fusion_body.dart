import 'package:chart_demo/syncfusion/components/bar_sync_fusion_content.dart';
import 'package:chart_demo/syncfusion/components/pie_sync_fusion_content.dart';
import 'package:chart_demo/syncfusion/components/stacked_line_sync_fusion_content.dart';
import 'package:flutter/material.dart';

class SyncFusionBody extends StatelessWidget {
  const SyncFusionBody({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: DefaultTabController(
        length: 3,
        child: Column(
          children: [
            TabBar(
              indicatorSize: TabBarIndicatorSize.tab,
              tabs: [
                Tab(text: "Bar"),
                Tab(text: "Pie"),
                Tab(text: "Stacked Line")
              ],
            ),
            Flexible(
              child: TabBarView(
                children: [
                  BarSyncFusionContent(),
                  PieSyncFusionContent(),
                  StackedLineSyncFusionContent(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
