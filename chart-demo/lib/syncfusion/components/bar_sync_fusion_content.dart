import 'package:chart_demo/syncfusion/data/data.dart';
import 'package:chart_demo/syncfusion/model/chart_data.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class BarSyncFusionContent extends StatelessWidget {
  const BarSyncFusionContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SfCartesianChart(
          series: <ChartSeries>[
            ColumnSeries<ChartData, String>(
              dataSource: Data.resultados,
              xValueMapper: (ChartData data, _) => data.x,
              yValueMapper: (ChartData data, _) => data.y,
              pointColorMapper: (ChartData data, _) => data.color,
            ),
          ],
          primaryXAxis: CategoryAxis(
            labelStyle: const TextStyle(
              fontSize: 10,
              fontWeight: FontWeight.bold,
              color: Colors.grey,
            ),
            majorGridLines: const MajorGridLines(width: 0),
          ),
          primaryYAxis: NumericAxis(
            interval: 5,
            labelStyle: const TextStyle(
              fontSize: 10,
              fontWeight: FontWeight.bold,
              color: Colors.grey,
            ),
          ),
          title: ChartTitle(
            text: 'Cantidad de Ventas por Producto - Octubre 2023',
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          tooltipBehavior: TooltipBehavior(
            enable: true,
            header: 'Cantidad de Ventas',
          ),
        ),
      ],
    );
  }
}
