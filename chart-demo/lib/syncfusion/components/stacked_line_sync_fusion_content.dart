import 'package:chart_demo/syncfusion/data/data.dart';
import 'package:chart_demo/syncfusion/model/datetime_data.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StackedLineSyncFusionContent extends StatelessWidget {
  const StackedLineSyncFusionContent({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SfCartesianChart(
          series: <ChartSeries>[
            StackedLineSeries<DatetimeData, DateTime>(
              dataSource: Data.resultadosWithDatetime,
              xValueMapper: (DatetimeData data, _) => data.x,
              yValueMapper: (DatetimeData data, _) => data.ys[0],
              legendItemText: "Camisas",
              groupName: "Camisas",
            ),
            StackedLineSeries<DatetimeData, DateTime>(
              dataSource: Data.resultadosWithDatetime,
              xValueMapper: (DatetimeData data, _) => data.x,
              yValueMapper: (DatetimeData data, _) => data.ys[1],
              legendItemText: "Zapatillas",
              groupName: "Zapatillas",
            ),
            StackedLineSeries<DatetimeData, DateTime>(
              dataSource: Data.resultadosWithDatetime,
              xValueMapper: (DatetimeData data, _) => data.x,
              yValueMapper: (DatetimeData data, _) => data.ys[2],
              legendItemText: "Casacas",
              groupName: "Casacas",
            ),
            StackedLineSeries<DatetimeData, DateTime>(
              dataSource: Data.resultadosWithDatetime,
              xValueMapper: (DatetimeData data, _) => data.x,
              yValueMapper: (DatetimeData data, _) => data.ys[3],
              legendItemText: "Pantalones",
              groupName: "Pantalones",
            ),
          ],
          palette: [
            Colors.yellow,
            Colors.green,
            Colors.blue,
            Colors.red,
            Colors.orange,
          ],
          primaryXAxis: DateTimeAxis(
            majorGridLines: const MajorGridLines(width: 0),
            intervalType: DateTimeIntervalType.months,
          ),
          tooltipBehavior: TooltipBehavior(
            enable: true,
            shouldAlwaysShow: true,
            builder: (data, _, __, ___, index) {
              DatetimeData d = data as DatetimeData;
              return Padding(
                padding: const EdgeInsets.all(5),
                child: Text(
                  'Cantidad de Ventas: ${d.ys[index]}',
                  style: const TextStyle(color: Colors.white),
                ),
              );
            },
          ),
          title: ChartTitle(
            text: 'Cantidad de Ventas por Producto y Meses',
            textStyle: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          legend: const Legend(
            isVisible: true,
            position: LegendPosition.bottom,
          ),
        ),
      ],
    );
  }
}
